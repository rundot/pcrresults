package ua.dp.rundot.pcrresult;

/**
 * Created by rundot on 06.03.2017.
 */
public enum ResultType {
    FEMOFLOR_SCR,
    FEMOFLOR_4,
    FEMOFLOR_16,
    ANDROFLOR,
    ANDROFLOR_SCR,
    TROMBO,
    INTERLEIKIN,
    BESPLOD
}
