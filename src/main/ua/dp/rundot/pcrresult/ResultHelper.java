package ua.dp.rundot.pcrresult;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rundot on 06.03.2017.
 */
public class ResultHelper {

    public static ResultType getResultType(String pageContent) {
        if (pageContent.contains("ФЕМОФЛОР СКРИН")) return ResultType.FEMOFLOR_SCR;
        if (pageContent.contains("Фемофлор 4")) return ResultType.FEMOFLOR_4;
        if (pageContent.contains("Фемофлор 16")) return ResultType.FEMOFLOR_16;
        if (pageContent.contains("Андрофлор® Скрин")) return ResultType.ANDROFLOR_SCR;
        if (pageContent.contains("Андрофлор®")) return ResultType.ANDROFLOR;
        if (pageContent.contains("ITGB3")) return ResultType.TROMBO;
        if (pageContent.contains("Определение делеций AZF")) return ResultType.BESPLOD;
        if (pageContent.contains("IL28B")) return ResultType.INTERLEIKIN;
        return null;
    }

    public static String pdfStringDecode(String pdfString) {
        Pattern p = Pattern.compile("[0-9A-F]+");
        String result = "";
        Matcher m = p.matcher(pdfString);
        if (!m.find()) return null;
        String encodeStr = m.group();
        if (encodeStr.length() < 4) return null;
        for (int i = 0; i < encodeStr.length(); i+= 4) {
            String seq = encodeStr.substring(i, i + 4);
            int code = Integer.parseInt(seq, 16);
            char c = (char)(code >= 560 ? code + 470 : code + 29);
            result += c;
        }
        System.out.printf("%s : %s%n", encodeStr, result);
        return result;
    }

    public static String pdfStringEncode(String regularString) {
        String result = "";
        for (int i = 0; i < regularString.length(); i++) {
            Character c = regularString.charAt(i);
            if (c.isAlphabetic(c)) {
                result += String.format("%4S", Integer.toHexString(c - 470));
            } else {
                result += String.format("%4S", Integer.toHexString(c - 29));
            }
        }
        return "<" + result.replaceAll(" ", "0") + "> Tj";
    }

    public static void main(String[] args) {
        System.out.println(pdfStringEncode("9.9"));
    }

}
