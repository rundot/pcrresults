package ua.dp.rundot.util;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * Created by rundot on 16.03.2017.
 */
public class SingleEntityManager {

    private static EntityManager em;

    private SingleEntityManager() {}

    public static EntityManager getEntityManager() {
        if (em == null) {
            em = Persistence.createEntityManagerFactory("RUNDOT").createEntityManager();
        }
        return em;
    }

}
