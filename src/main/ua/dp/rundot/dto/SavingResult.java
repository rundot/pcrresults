package ua.dp.rundot.dto;

import ua.dp.rundot.domain.Result;
import ua.dp.rundot.pcrresult.ResultType;
import ua.dp.rundot.service.ResultService;

/**
 * Created by rundot on 14.03.2017.
 */
public class SavingResult {

    private int inz;
    private boolean obm;
    private boolean kvm;
    private String filename;
    private ResultType resultType;

    public ResultType getResultType() {
        return resultType;
    }

    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }

    public int getInz() {
        return inz;
    }

    public SavingResult(int inz) {
        this.inz = inz;
    }

    public void setInz(int inz) {
        this.inz = inz;
    }

    public boolean isObm() {
        return obm;
    }

    public void setObm(boolean obm) {
        this.obm = obm;
    }

    public boolean isKvm() {
        return kvm;
    }

    public void setKvm(boolean kvm) {
        this.kvm = kvm;
    }

    public String getFilename() {
        Result result = ResultService.getByInz(inz);
        return String.format("%d_%s_(%s).pdf", result.getInz(), result.getPatient(), result.getType());
    }

    @Override
    public String toString() {
        return "SavingResult{" +
                "inz=" + inz +
                ", obm=" + obm +
                ", kvm=" + kvm +
                ", resultType=" + resultType +
                '}';
    }
}
