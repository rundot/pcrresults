package ua.dp.rundot.controller;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.canvas.parser.PdfCanvasProcessor;
import com.itextpdf.kernel.pdf.canvas.parser.listener.LocationTextExtractionStrategy;
import ua.dp.rundot.domain.Result;
import ua.dp.rundot.parser.ResultParserFactory;
import ua.dp.rundot.pcrresult.ResultHelper;
import ua.dp.rundot.pcrresult.ResultType;
import ua.dp.rundot.service.ResultService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rundot on 13.03.2017.
 */
@WebServlet("/")
@MultipartConfig
public class FileUploadServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/upload.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part filePart = req.getPart("uploadData");
        String filename = filePart.getSubmittedFileName();
        InputStream fileContent = filePart.getInputStream();
        PdfDocument pdfDocument = new PdfDocument(new PdfReader(fileContent));

        List<Result> pcrResultList = new ArrayList<>();

        for (int pageNum = 1; pageNum <= pdfDocument.getNumberOfPages(); pageNum++ ) {
            LocationTextExtractionStrategy strategy = new LocationTextExtractionStrategy();
            PdfCanvasProcessor parser = new PdfCanvasProcessor(strategy);
            parser.processPageContent(pdfDocument.getPage(pageNum));
            String pageContent = strategy.getResultantText();
            ResultType resultType = ResultHelper.getResultType(pageContent);
            Result pcrResult = ResultParserFactory.getParser(resultType).parse(pageContent);
            pcrResult.setStream(pdfDocument.getPage(pageNum).getFirstContentStream().getBytes());
            ResultService.add(pcrResult);
            pcrResultList.add(pcrResult);
        }
        req.setAttribute("filename", filename);
        req.setAttribute("resultList", pcrResultList);
        req.getRequestDispatcher("jsp/process.jsp").forward(req, resp);
    }
}
