package ua.dp.rundot.controller;

import ua.dp.rundot.dto.SavingResult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by rundot on 17.03.2017.
 */
@WebServlet("/save")
public class SaveServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("back".equals(req.getParameter("action"))) {
            resp.sendRedirect("/pcr");
            return;
        }
        if ("save".equals(req.getParameter("action"))) {
            Map<String, String[]> params = req.getParameterMap();
            List<SavingResult> resultList = new ArrayList<>();

            List<Integer> activeList = params.get("active") != null ?
                    Arrays.asList(params.get("active"))
                            .stream()
                            .map(Integer::parseInt)
                            .collect(Collectors.toList()) : new ArrayList<>();

            List<Integer> obmList = params.get("obm") != null ?
                    Arrays.asList(params.get("obm"))
                            .stream()
                            .map(Integer::parseInt)
                            .collect(Collectors.toList()) : new ArrayList<>();
            List<Integer> kvmList = params.get("kvm") != null ?
                    Arrays.asList(params.get("kvm"))
                            .stream()
                            .map(Integer::parseInt)
                            .collect(Collectors.toList()) : new ArrayList<>();

            activeList
                    .stream()
                    .forEach(inz -> {
                        SavingResult savingResult = new SavingResult(inz);
                        if (obmList.contains(inz)) savingResult.setObm(true);
                        if (kvmList.contains(inz)) savingResult.setKvm(true);
                        resultList.add(savingResult);
                    });
//            resp.getWriter().write(resultList.toString());
            req.setAttribute("resultList", resultList);
            req.getRequestDispatcher("/jsp/save.jsp").forward(req, resp);
        }
    }
}
