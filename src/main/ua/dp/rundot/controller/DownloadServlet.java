package ua.dp.rundot.controller;

import ua.dp.rundot.constructor.ResultConstructor;
import ua.dp.rundot.constructor.ResultConstructorFactory;
import ua.dp.rundot.domain.Result;
import ua.dp.rundot.service.ResultService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by rundot on 17.03.2017.
 */
@WebServlet("/download")
public class DownloadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int inz = Integer.parseInt(req.getParameter("inz"));
        boolean kvm = Boolean.parseBoolean(req.getParameter("kvm"));
        boolean obm = Boolean.parseBoolean(req.getParameter("obm"));
        Result result = ResultService.getByInz(inz);
        ResultConstructor resultConstructor = ResultConstructorFactory.getResultConstructor(result.getType());
        resultConstructor.setKvm(kvm);
        resultConstructor.setObm(obm);
        resultConstructor.setResult(result);
        byte[] file = resultConstructor.construct();
        resp.getWriter().write(new String(file));

    }
}
