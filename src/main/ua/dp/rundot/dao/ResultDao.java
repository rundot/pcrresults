package ua.dp.rundot.dao;

import ua.dp.rundot.domain.Result;

import java.util.List;

/**
 * Created by rundot on 16.03.2017.
 */
public interface ResultDao {

    Result add(Result result);

    void delete(long id);

    Result get(long id);

    void update(Result result);

    List<Result> getAll();

    Result getByInz(int inz);

}
