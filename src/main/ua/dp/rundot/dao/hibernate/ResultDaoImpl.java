package ua.dp.rundot.dao.hibernate;

import ua.dp.rundot.dao.ResultDao;
import ua.dp.rundot.domain.Result;
import ua.dp.rundot.util.SingleEntityManager;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by rundot on 16.03.2017.
 */
public class ResultDaoImpl implements ResultDao {

    private EntityManager em = SingleEntityManager.getEntityManager();

    @Override
    public Result add(Result result) {
        if (getByInz(result.getInz()) == null) {
            em.getTransaction().begin();
            Result dbResult = em.merge(result);
            em.getTransaction().commit();
            return dbResult;
        }
        return null;
    }

    @Override
    public void delete(long id) {
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    @Override
    public Result get(long id) {
        return em.find(Result.class, id);
    }

    @Override
    public void update(Result result) {
        em.getTransaction().begin();
        em.merge(result);
        em.getTransaction().commit();
    }

    @Override
    public Result getByInz(int inz) {
        TypedQuery<Result> namedQuery = em.createNamedQuery("Result.getByInz", Result.class);
        namedQuery.setParameter("inz", inz);
        return namedQuery.getSingleResult();
    }

    @Override
    public List<Result> getAll() {
        TypedQuery<Result> namedQuery = em.createNamedQuery("Result.getAll", Result.class);
        return namedQuery.getResultList();
    }

    public static void main(String[] args) {
        ResultDaoImpl resultDao = new ResultDaoImpl();
        Result result = resultDao.getByInz(123456789);
        System.out.println(result);
    }

}
