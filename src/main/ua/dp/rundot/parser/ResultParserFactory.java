package ua.dp.rundot.parser;

import ua.dp.rundot.parser.impl.*;
import ua.dp.rundot.pcrresult.ResultType;

/**
 * Created by rundot on 06.03.2017.
 */
public class ResultParserFactory {

    private ResultParserFactory() {}

    public static ResultParser getParser(ResultType resultType) {
        switch (resultType) {
            case FEMOFLOR_4: return new Femoflor4Parser();
            case FEMOFLOR_16: return new Femoflor16Parser();
            case FEMOFLOR_SCR: return new FemoflorScrParser();
            case ANDROFLOR: return new AndroflorParser();
            case ANDROFLOR_SCR: return new AndroflorScrParser();
            case BESPLOD: return new BesplodParser();
            case INTERLEIKIN: return new InterleikinParser();
            case TROMBO: return new TromboParser();
        }
        return null;
    }

}
