package ua.dp.rundot.parser;

import ua.dp.rundot.domain.Result;

/**
 * Created by rundot on 06.03.2017.
 */
public interface ResultParser {

    Result parse(String pageContent);

}
