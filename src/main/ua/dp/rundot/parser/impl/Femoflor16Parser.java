package ua.dp.rundot.parser.impl;

import ua.dp.rundot.domain.Result;
import ua.dp.rundot.parser.ResultParser;
import ua.dp.rundot.pcrresult.ResultType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rundot on 06.03.2017.
 */
public class Femoflor16Parser implements ResultParser {

    @Override
    public Result parse(String pageContent) {
        String[] lines = pageContent.split("\n");
        String[] patientStr = lines[5].split("Ф\\.И\\.О\\. пациента");
        String patient = patientStr.length == 2 ? patientStr[1].trim().toUpperCase().replaceAll("\\.", "") : "НЕИЗВЕСТНЫЙ";
        String inzStr = lines[4];
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(inzStr);
        int inz = 0;
        if (m.find()) {
            inz = Integer.parseInt(m.group());
        }
        return new Result(inz, patient, ResultType.FEMOFLOR_16);
    }
}
