package ua.dp.rundot.domain;

import ua.dp.rundot.pcrresult.ResultType;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by rundot on 16.03.2017.
 */

@Entity
@Table(name="results")
@NamedQueries({
        @NamedQuery(name = "Result.getAll", query = "SELECT r FROM Result r"),
        @NamedQuery(name = "Result.getByInz", query = "SELECT r FROM Result r where r.inz = :inz")
})
public class Result {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "inz", unique = true)
    private Integer inz;

    @Column(name = "patient")
    private String patient;

    @Column(name = "type")
    private ResultType type;

    @Column(name = "stream")
    private byte[] stream;

    public Result() {}

    public Result(int inz, String patient, ResultType type, byte[] stream) {
        this.inz = inz;
        this.patient = patient;
        this.type = type;
        this.stream = stream;
    }

    public Result(int inz, String patient, ResultType type) {
        this.inz = inz;
        this.patient = patient;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public Integer getInz() {
        return inz;
    }

    public void setInz(int inz) {
        this.inz = inz;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public ResultType getType() {
        return type;
    }

    public void setType(ResultType type) {
        this.type = type;
    }

    public byte[] getStream() {
        return stream;
    }

    public void setStream(byte[] stream) {
        this.stream = stream;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", inz='" + inz + '\'' +
                ", patient='" + patient + '\'' +
                ", type='" + type + '\'' +
                ", stream=" + Arrays.toString(stream) +
                '}';
    }
}
