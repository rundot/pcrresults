package ua.dp.rundot.constructor;

import ua.dp.rundot.domain.Result;

/**
 * Created by rundot on 09.03.2017.
 */
public interface ResultConstructor {

    void setHeader(String headerPath);

    void setBackground(String backgroundPath);

    void setObm(boolean obm);

    void setKvm(boolean kvm);

    void setResult(Result result);

    byte[] construct();

}
