package ua.dp.rundot.constructor;

import ua.dp.rundot.constructor.impl.AndroflorConstructor;
import ua.dp.rundot.constructor.impl.FemoflorScrConstructor;
import ua.dp.rundot.pcrresult.ResultType;

/**
 * Created by rundot on 17.03.2017.
 */
public class ResultConstructorFactory {

    private ResultConstructorFactory() {}

    public static ResultConstructor getResultConstructor(ResultType resultType) {
        switch (resultType) {
            case FEMOFLOR_SCR: return new FemoflorScrConstructor();
            case ANDROFLOR: return new AndroflorConstructor();
        }
        return null;
    }

}
