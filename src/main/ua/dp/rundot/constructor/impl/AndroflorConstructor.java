package ua.dp.rundot.constructor.impl;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfStream;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import ua.dp.rundot.constructor.ResultConstructor;
import ua.dp.rundot.domain.Result;
import ua.dp.rundot.pcrresult.ResultHelper;

import java.net.MalformedURLException;

/**
 * Created by rundot on 09.03.2017.
 */
public class AndroflorConstructor implements ResultConstructor {

    private static final String RESULT_DIR = "C:\\Temp\\OUT";
    private ImageData background;
    private ImageData header;

    private String patient;

    @Override
    public void setResult(Result result) {

    }

    private int inz;
    PdfPage pdfPage;

    public AndroflorConstructor(PdfPage pdfPage, String patient, int inz) {
        this.patient = patient;
        this.inz = inz;
        this.pdfPage = pdfPage;
    }

    @Override
    public void setHeader(String headerPath) {
        try {
            this.header = ImageDataFactory.create(headerPath);
        } catch (MalformedURLException e) {
        }
    }

    @Override
    public void setBackground(String backgroundPath) {
        try {
            this.background = ImageDataFactory.create(backgroundPath);
        } catch (MalformedURLException e) {
        }
    }

    @Override
    public void setObm(boolean obm) {

    }

    public AndroflorConstructor() {
    }

    @Override
    public void setKvm(boolean kvm) {

    }

    @Override
    public byte[] construct() {
//        try {
//
//            PdfWriter writer = new PdfWriter(filename);
//            PdfDocument document = new PdfDocument(writer);
//            PdfStream stream = pdfPage.getFirstContentStream();
//
//            setHeader("C:/Temp/FEMO/header.png");
//
//            //Чистим "вручную" стрим страницы
//            stream.setData(
//                    new String(stream.getBytes())
//                            .replaceAll(ResultHelper.pdfStringEncode("нд"), ResultHelper.pdfStringEncode("не выявлено")) //Меняем надписи "нд" на "не выявлено"
//                            .replaceAll("337.47", "318.09") //... и сдвигаем его, так как надпись надо центрировать
//                            .getBytes()
//            );
//
//            PdfCanvas canvas = new PdfCanvas(document.addNewPage());
//            if (background != null)
//                canvas.addImage(background, 0, 0, PageSize.A4.getWidth(), false);
//            if (header != null)
//                canvas.addImage(header, 0, PageSize.A4.getHeight() - header.getHeight() / 2, PageSize.A4.getWidth(), false);
//            canvas.addXObject(pdfPage.copyAsFormXObject(document), 0, -60);
//            document.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return null;
    }
}
