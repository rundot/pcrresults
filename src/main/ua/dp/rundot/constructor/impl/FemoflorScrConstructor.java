package ua.dp.rundot.constructor.impl;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfStream;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import ua.dp.rundot.constructor.ResultConstructor;
import ua.dp.rundot.domain.Result;
import ua.dp.rundot.pcrresult.ResultHelper;

import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;

/**
 * Created by rundot on 09.03.2017.
 */
public class FemoflorScrConstructor implements ResultConstructor{

    private ImageData background;
    private ImageData header;
    private Result result;
    private boolean obm;
    private boolean kvm;

    @Override
    public void setObm(boolean obm) {
        this.obm = obm;
    }

    @Override
    public void setKvm(boolean kvm) {
        this.kvm = kvm;
    }

    @Override
    public void setHeader(String headerPath) {
        try {
            this.header = ImageDataFactory.create(headerPath);
        } catch (MalformedURLException e) {
        }
    }

    @Override
    public void setBackground(String backgroundPath) {
        try {
            this.background = ImageDataFactory.create(backgroundPath);
        } catch (MalformedURLException e) {
        }
    }

    @Override
    public void setResult(Result result) {
        this.result = result;
    }

    public byte[] construct() {
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            PdfWriter writer = new PdfWriter(bout);
            PdfDocument document = new PdfDocument(writer);
            byte[] stream = result.getStream();
            PdfStream pdfStream = new PdfStream(stream);
            if (obm) {
                stream = new String(stream)
                        .replaceAll("1 1 1 rg\r\n378.35 598.90", "1 0 0 rg\r\n378.35 598.90")
                        .getBytes();
            }
            if (kvm) {
                stream = new String(stream)
                        .replaceAll("355.89 613.60 Td\r\n<.{4,12}> Tj", "355.89 613.60 Td\r\n" + ResultHelper.pdfStringEncode("4.0"))
                        .getBytes();
            }
            //Чистим "вручную" стрим страницы
            pdfStream.setData(
                    new String(stream)
                            .replaceAll(ResultHelper.pdfStringEncode("нд"), ResultHelper.pdfStringEncode("не выявлено")) //Меняем надписи "нд" на "не выявлено"
                            .replaceAll("337.47", "318.09") //... и сдвигаем его, так как надпись надо центрировать
                            .replaceFirst(ResultHelper.pdfStringEncode("Дата"), ResultHelper.pdfStringEncode("ТМП")) //Меняем первую "Дата" на "ТМП"
                            .replaceAll(ResultHelper.pdfStringEncode("Дата"), ResultHelper.pdfStringEncode("")) //Удаляем вторую "Дата"
                            .replaceAll(ResultHelper.pdfStringEncode("ТМП"), ResultHelper.pdfStringEncode("Дата")) //Меняем "ТМП" на "Дата"
                            .replaceAll(ResultHelper.pdfStringEncode("Исследование выполнил"), ResultHelper.pdfStringEncode("")) //Удаляем надписи после таблицы
                            .replaceAll(ResultHelper.pdfStringEncode("Подпись"), ResultHelper.pdfStringEncode("")) //Туда же
                            .replaceAll("(0\\.76|1\\.00 0\\.00 0\\.50)(.|\\s){0,30}(466|454|442|430|418|406|394)(.|\\s){0,30}re", "") //Удаляем полоски для качественных исследований
                            .getBytes()
            );

            PdfCanvas canvas = new PdfCanvas(document.addNewPage());
            if (background != null)
                canvas.addImage(background, 0, 0, PageSize.A4.getWidth(), false);
            if (header != null)
                canvas.addImage(header, 0, PageSize.A4.getHeight() - header.getHeight() / 4, PageSize.A4.getWidth(), false);
            document.getFirstPage().newContentStreamAfter().setData(pdfStream.getBytes());
            PdfFormXObject obj = new PdfFormXObject(pdfStream);
            canvas.addXObject(obj, 0, -60);
            document.close();
            return bout.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
