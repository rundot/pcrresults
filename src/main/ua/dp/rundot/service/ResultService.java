package ua.dp.rundot.service;

import ua.dp.rundot.dao.ResultDao;
import ua.dp.rundot.dao.hibernate.ResultDaoImpl;
import ua.dp.rundot.domain.Result;

import java.util.List;

/**
 * Created by rundot on 16.03.2017.
 */
public class ResultService {

    private static ResultDao resultDao = new ResultDaoImpl();

    public static Result add(Result result) {
        return resultDao.add(result);
    }

    public static void delete(long id) {
        resultDao.delete(id);
    }

    public static Result get(long id) {
        return resultDao.get(id);
    }

    public static void update(Result result) {
        resultDao.update(result);
    }

    public static List<Result> getAll() {
        return resultDao.getAll();
    }

    public static Result getByInz(int inz) {
        return resultDao.getByInz(inz);
    }

}
