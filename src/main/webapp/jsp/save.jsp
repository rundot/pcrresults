<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!@DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <h1>Скачать файлы</h1>
            <form method="POST" action="#">
                <table>
                    <thead>
                        <tr>
                            <th>Выбрать</th>
                            <th>Имя файла</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="result" items="${resultList}">
                            <tr>
                                <td><input type="checkbox" name="selected" vale="${result.inz}" checked="checked" /></td>
                                <td><a href="/pcr/download?inz=${result.inz}&obm=${result.obm}&kvm=${result.kvm}">${result.filename}</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                Или отправить по электронной почте <input type="text" name="email" placeholder="user@mail.com" /> <button name="action">Отправить</button>
            </form>
    </body>
</html>