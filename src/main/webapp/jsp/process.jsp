<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!@DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <form method="POST" action="/pcr/save">
            <h1>Обработка файла: ${filename}</h1>
            <table>
                <thead>
                    <tr>
                        <th>Обрабатывать</th>
                        <th>ИНЗ</th>
                        <th>Пациент</th>
                        <th>Исследование</th>
                        <th>ОБМ</th>
                        <th>КВМ</th>
                        <th>Вариант бланка</th>
                        <th>Заказчик</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="result" items="${resultList}">
                        <tr>
                            <input type="hidden" name="inz" value="${result.inz}" />
                            <td><input type="checkbox" name="active" value="${result.inz}" checked="checked" />
                            <td>${result.inz}</td>
                            <td>${result.patient}</td>
                            <td>${result.type}</td>
                            <td><input type="checkbox" name="obm" value="${result.inz}" />
                            <td><input type="checkbox" name="kvm" value="${result.inz}" />
                            <td>
                                <select name="blank">
                                    <option selected="selected">Стандартный</option>
                                    <option>Корпоративный</option>
                                    <option>Специальный</option>
                                </select></td>
                            <td></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <button name="action" value="save">Сохранить</button>
            <button name="action" value="back">Назад</button>
        </form>
    </body>
</html>